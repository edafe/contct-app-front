import Vue from 'vue'
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import App from './App.vue'

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.http.options.root = 'https://vue-http-b5504.firebaseio.com/data.json'


Vue.filter('toLowercase', function(value){
	return value.toLowerCase();
});

new Vue({
  el: '#app',
  render: h => h(App)
})
